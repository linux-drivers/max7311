KDIR ?= /media/arty/f39e44b0-ddc5-4497-87be-9cd04a6dfe7d/cad_files/poky_sumo/build_qt_efls/tmp/work/cyclone5-poky-linux-gnueabi/linux-altera-lts/5.4.64-lts+gitAUTOINC+1f2f1ded05-r0/linux-cyclone5-standard-build


default:
	$(MAKE) -C $(KDIR) ARCH=arm M=$(CURDIR)

clean:
	$(MAKE) -C $(KDIR) ARCH=arm M=$(CURDIR) clean

help:
	$(MAKE) -C $(KDIR) ARCH=arm M=$(CURDIR) help

modules:
	$(MAKE) -C $(KDIR) ARCH=arm M=$(CURDIR) modules

modules_install:
	$(MAKE) -C $(KDIR) ARCH=arm M=$(CURDIR) modules_install
