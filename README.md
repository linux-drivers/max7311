# Driver for IC max7311 - 2-Wire-Interfaced 16-Bit I/O Port Expander with Interrupt

# Description

Datasheet [https://eu.mouser.com/datasheet/2/256/max7311-1178346.pdf](https://eu.mouser.com/datasheet/2/256/max7311-1178346.pdf)

Driver represents IC max7311 IO pins as general gpio pins in linux.
Also driver fully supports interrupts for gpio configured as inputs.

# Device tree binding

**port-config** - default port IO configuration for port. Each bit represents one IC pin.

```c
0 - output
1 - input
port1-config - I/O pins [7..0]	
port2-config - I/O pins [15..8]
```

**out-default** - default output values on driver probing.

```c
1 - high level
0 - low level
out1-default - I/O pins [7..0]
out1-default - I/O pins [15..0]
```

**interrupt-parent** - parent interrupt controller.

**interrupts**  - HPS pin number, first cell; interrupt type, second cell.

**reg** - i2c 7bit address.

## Example:

```c
i2c_mux: max7311atg@0x20 {
	compatible = "maxim,max7311atg";
	reg = <0x00000020>;
	port1-config = <0x00>;
	port2-config = <0xFE>;
	out1-default = <0x01>;
	out2-default = <0x00>;
	interrupt-parent = <&hps_gpio2_porta>;
	interrupts = <18 2>;
	gpio-controller;
	#gpio-cells = <2>;
	interrupt-controller;
	#interrupt-cells = <2>;
}; //end max311atg@0x20 (i2c_mux)
```

After driver probes, we'll have new gpiochip, representing states of IC port-expander pins.
Then, we can add sub-nodes, for inputs "gpio-keys", for example - outputs **"gpio-leds"**,  **"gpio-poweroff" or “gpio-keys”** for inputs.

## Example:

### Inputs

```c
mux_inputs: mux_inputs {
compatible = "gpio-keys";
#address-cells = <1>;
#size-cells = <0>;

	mux_btn_13: pg_33 {
		label = "pg_33_reg13_F5";
		gpios = <&i2c_mux 13 1>; // pin number 13, active-low
		linux,code = <63>;
	};
		
	mux_btn_11: btn_int {
		label = "btn_int_reg11_F6";
		gpios = <&i2c_mux 11 1>; // pin number 11, active-low
		linux,code = <64>;
	};
};
```

### Poweroff

```c
poweroff_gpio: poweroff_gpio {
	compatible = "gpio-poweroff";
	gpios = <&i2c_mux 0 1>; // pin number 0
	timeout-ms = <3000>;
};//end (poweroff-gpio)
```
