/* i2c-max7311atg: I2C driver.
 *
 * ArtyPi <temaprv@protonmail.com>
 *
 * 2-Wire-Interfaced 16-Bit I/O Port Expander with Interrupt and Hot-Insertion Protection
 *
 * Datasheet https://eu.mouser.com/datasheet/2/256/max7311-1178346.pdf
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */

#include <linux/gpio/driver.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/irq.h>
#include <linux/irqdomain.h>
#include <linux/mutex.h>
#include <linux/slab.h>

/*
 * Registers description.
 */

/* Default states XX */
#define MUX_REG_R_PORT1_IN      (0x00)
#define MUX_REG_R_PORT2_IN      (0x01)

/* Default states 1 - outputs */
#define MUX_REG_RW_PORT1_OUT    (0x02)
#define MUX_REG_RW_PORT2_OUT    (0x03)

/* Default states 0 - original polarity */
#define MUX_REG_RW_PORT1_POL    (0x04)
#define MUX_REG_RW_PORT2_POL    (0x05)

/* Default states 1 - all pins are input */
#define MUX_REG_RW_PORT1_CONFIG (0x06)
#define MUX_REG_RW_PORT2_CONFIG (0x07)

/* Default states 1 - timeout enabled */
#define MUX_REG_W_TIMEOUT_ENA   (0x08)

#define PIN_NUMBER (16)

struct max7311 {
    struct device       *dev;
    struct gpio_chip    chip;
    struct mutex        lock;
    u16                 port_config;
    u16                 out_level;
    u16                 in_level;

    struct mutex        irq_lock;
    int                 irq_base;
    struct irq_domain	*irq_domain;
    u16                 irq_mask;
    u16                 irq_mask_cur;
    u16                 irq_trig_raise;
    u16                 irq_trig_fall;

    int (*write)(struct device *dev, u8 reg, u8 *buf, u8 len);
    int (*read)(struct device *dev, u8 reg, u8 *val, int len);
};

struct max7311_platform_data {
    unsigned    base;
    u32         port1_config;
    u32         port2_config;
    u32         out1_default;
    u32         out2_default; 
};

/* This lock class tells lockdep that GPIO irqs are in a different
* category than their parents, so it won't report false recursion.
*/
static struct lock_class_key gpio_lock_class;


static int max7311_i2c_write(struct device *dev, u8 reg, u8 *buf, u8 len)
{
    int ret;
    u8 b[256];
    struct i2c_client *client = to_i2c_client(dev);
    struct i2c_msg msg;

    b[0] = reg;
    memcpy(&b[1],buf,len);

    msg.addr = client->addr;
    msg.flags = 0;

    msg.buf = b;
    msg.len = len + 1;

    ret = i2c_transfer(client->adapter, &msg, len);
    if (ret >= 0)
        ret = 0;
    else
        ret = -EREMOTEIO;

    return ret;
}

static int max7311_i2c_read(struct device *dev, u8 reg, u8 *val, int len)
{
    int ret;
    struct i2c_client *client = to_i2c_client(dev);
    struct i2c_msg msg[2];

    msg[0].addr = client->addr;
    msg[0].flags = 0;
    msg[0].buf = &reg;
    msg[0].len = 1;

    msg[1].addr = client->addr;
    msg[1].flags = I2C_M_RD;
    msg[1].buf = val;
    msg[1].len = len;

    ret = i2c_transfer(client->adapter, msg, 2);
    if (ret == 2) {
        ret = 0;
    } else {
        printk("i2c msg failed=%d\n",ret);
        ret = -EREMOTEIO;
    }

    return ret;
}

static int max7311_direction_input(struct gpio_chip *chip, unsigned offset)
{    
    struct max7311 *ts = container_of(chip, struct max7311, chip);
    u16 pin_mask;
    u8 adr, pin_config;
    int ret;

    pin_mask = ~(1 << offset);
    if (offset < 8) {
        ts->port_config = (ts->port_config & pin_mask) | (1 << offset);
	pin_config = ts->port_config & 0xFF;
        adr = MUX_REG_RW_PORT1_CONFIG;
    } else {
        ts->port_config = (ts->port_config & pin_mask) | (1 << offset);
	pin_config = (ts->port_config >> 8) & 0xFF;
        adr = MUX_REG_RW_PORT2_CONFIG;
    }
    mutex_lock(&ts->lock);

    ret = ts->write(ts->dev, adr, &pin_config, 1);

    mutex_unlock(&ts->lock);

    return ret;
}

static int __max7311_set(struct max7311 *ts, unsigned offset, int value)
{
    int ret;
    u8 lvl;

    if (value) {
        ts->out_level |= 1 << offset;
    } else {
        ts->out_level &= ~(1 << offset);
    }

    lvl = ts->out_level & 0xFF;
    ret = ts->write(ts->dev, MUX_REG_RW_PORT1_OUT, &lvl, 1);
    lvl = ( ts->out_level >> 8 ) & 0xFF;
    ret = ts->write(ts->dev, MUX_REG_RW_PORT2_OUT, &lvl, 1);

    return ret;
}

static int max7311_setb(struct max7311 *ts, u8 reg, u8 value)
{
    return ts->write(ts->dev, reg, &value, 1);
}

static int max7311_direction_output(struct gpio_chip *chip, unsigned offset, int value)
{
    struct max7311 *ts = container_of(chip, struct max7311, chip);
    u16 pin_mask;
    u8 adr, pin_config;
    int ret;
    pin_mask = ~(1 << offset);

    if(offset < 8) {
        ts->port_config = ts->port_config & pin_mask;
	pin_config = ts->port_config & 0xFF;
        adr = MUX_REG_RW_PORT1_CONFIG;
    } else {
        ts->port_config = ts->port_config & pin_mask ;
	pin_config = (ts->port_config >> 8) & 0xFF; 
        adr = MUX_REG_RW_PORT2_CONFIG;
    }

    mutex_lock(&ts->lock);

    ret = __max7311_set(ts, offset, value);

    if (ret >= 0) {
        ret = ts->write(ts->dev, adr, &pin_config, 1);
    }
    mutex_unlock(&ts->lock);

    return ret;
}

static int max7311_get(struct gpio_chip *chip, unsigned offset)
{    
    struct max7311 *ts = container_of(chip, struct max7311, chip);
    int config;
    u8 level, lvl[2];
    int ret;

    ret = -EINVAL;

    mutex_lock(&ts->lock);

    config = ts->port_config & (1 << offset);

    if (config) {
	/* Input: read out */
        ret = ts->read(ts->dev, MUX_REG_R_PORT1_IN, &lvl[0], 1);
        ret = ts->read(ts->dev, MUX_REG_R_PORT2_IN, &lvl[1], 1);
        ts->in_level = (lvl[1] << 8) | lvl[0];

        if (offset < 8) {
            level = lvl[0];
        } else {
            level = lvl[1];
            offset -= 8;
        }
        level = (level >> offset) & 0x01;        

    } else {
       /* Output: return cached level */
        level =  (ts->out_level & (1 << offset)) >> offset; 
    }
    mutex_unlock(&ts->lock);

    return level;
}

static u8 max7311_getb(struct gpio_chip *chip, u8 port_number)
{
    u8 level, adr;
    struct max7311 *ts = container_of(chip, struct max7311, chip);

    if (port_number == 1) {
        adr = MUX_REG_R_PORT1_IN;
    } else if (port_number == 2) {
        adr = MUX_REG_R_PORT2_IN;
    }
    else
        return 0;

    mutex_lock(&ts->lock);

    ts->read(ts->dev, adr, &level, 1);

    mutex_unlock(&ts->lock);

    return level;
}

static void max7311_set(struct gpio_chip *chip, unsigned offset, int value)
{
    struct max7311 *ts = container_of(chip, struct max7311, chip);

    mutex_lock(&ts->lock);

    __max7311_set(ts, offset, value);

    mutex_unlock(&ts->lock);
}

static void max7311_irq_update_mask(struct max7311 *ts)
{    
    if (ts->irq_mask == ts->irq_mask_cur)
        return;

    ts->irq_mask = ts->irq_mask_cur; 
}

static int max7311_gpio_to_irq(struct gpio_chip *chip, unsigned off)
{
    struct max7311 *ts = container_of(chip, struct max7311, chip);

    return irq_find_mapping(ts->irq_domain, off);
}

static void max7311_irq_mask(struct irq_data *d)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(d);
    unsigned int pos = d->hwirq;

    ts->irq_mask_cur &= ~(1 << pos);
}

static void max7311_irq_unmask(struct irq_data *d)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(d);
    unsigned int pos = d->hwirq;

    ts->irq_mask_cur |= 1 << pos;
}

static void max7311_irq_bus_lock(struct irq_data *d)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(d);

    mutex_lock(&ts->irq_lock);
    ts->irq_mask_cur = ts->irq_mask;
}

static void max7311_irq_bus_sync_unlock(struct irq_data *d)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(d);

    max7311_irq_update_mask(ts);
    mutex_unlock(&ts->irq_lock);
}

static int max7311_irq_set_type(struct irq_data *d, unsigned int type)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(d);
    struct i2c_client *client = to_i2c_client(ts->dev);
    unsigned int off = d->hwirq;
    u16 mask;

    mask = 1 << off;

    if (!(mask & ts->port_config)) {
        dev_dbg(&client->dev, "%s port %d is output only\n",
            client->name, off);
        return -EACCES;
    }

    if (!(type & IRQ_TYPE_EDGE_BOTH)) {
        dev_err(&client->dev, "IRQ %d: unsupported type %d\n",
            d->irq, type);
        return -EINVAL;
    }

    if (type & IRQ_TYPE_EDGE_FALLING)
        ts->irq_trig_fall |= mask;
    else
        ts->irq_trig_fall &= ~mask;

    if (type & IRQ_TYPE_EDGE_RISING)
        ts->irq_trig_raise |= mask;
    else
        ts->irq_trig_raise &= ~mask;

    return 0;
}

static unsigned int max7311_irq_startup(struct irq_data *data)
{
    struct max7311 *ts = irq_data_get_irq_chip_data(data);

    if (gpiochip_lock_as_irq(&ts->chip, data->hwirq)) {
        dev_err(ts->dev,
                "Unable to lock HW IRQ %lu for IRQ usage\n",
                data->hwirq);
    }
    max7311_irq_unmask(data);

    return 0;
}

static struct irq_chip max7311_irq_chip = {
    .name                   = "max7311",
    .irq_mask               = max7311_irq_mask,
    .irq_unmask             = max7311_irq_unmask,
    .irq_bus_lock           = max7311_irq_bus_lock,
    .irq_bus_sync_unlock    = max7311_irq_bus_sync_unlock,
    .irq_set_type           = max7311_irq_set_type,
    .irq_startup            = max7311_irq_startup,
};

static u16 max7311_irq_pending(struct max7311 *ts)
{
    u16 cur_stat;
    u16 old_stat;
    u16 trigger;
    u16 pending;    
    u8 value;
    u16 pend_rising;
    u16 pend_falling;

    old_stat = ts->in_level;

    value = max7311_getb(&ts->chip, 1);
    cur_stat = (max7311_getb(&ts->chip, 2) << 8) | value;

    ts->in_level = cur_stat;

    trigger = cur_stat ^ old_stat; 
    trigger &= ts->irq_mask;

    pend_rising  = cur_stat & ts->irq_trig_raise; 
    pend_falling = old_stat & ts->irq_trig_fall; 
    pending = pend_rising | pend_falling;
    pending &= trigger;

    return pending;
}

static irqreturn_t max7311_irq_handler(int irq, void *devid)
{
    struct max7311 *ts = devid;
    u16 pending;
    u8 level;
    unsigned int child_irq;

    pending = max7311_irq_pending(ts);

    if (!pending)
        return IRQ_HANDLED;

    do {
        level = __ffs(pending);
        child_irq = irq_find_mapping(ts->irq_domain, level);
        handle_nested_irq(child_irq);
        pending &= ~(1 << level);
    } while (pending);

    return IRQ_HANDLED;
}

static int max7311_irq_setup(struct max7311 *ts)
{
    int ret, j, irq;
    struct gpio_chip *chip = &ts->chip;

    mutex_init(&ts->irq_lock);

    ts->irq_mask        = 0x00;
    ts->irq_mask_cur    = 0x00;
    ts->irq_trig_fall   = 0x00;
    ts->irq_trig_raise  = 0x00;

    ts->irq_domain = irq_domain_add_linear(chip->of_node, chip->ngpio,
                        &irq_domain_simple_ops, ts);
    if (!ts->irq_domain)
        return -ENODEV;

    ret = devm_request_threaded_irq(ts->dev, ts->irq_base, NULL, max7311_irq_handler,
                    IRQF_TRIGGER_LOW | IRQF_ONESHOT,
                    dev_name(ts->dev), ts);

    if (ret != 0) {
        dev_err(ts->dev, "Unable to request IRQ#%d: %d\n",
            ts->irq_base, ret);
        return ret;
    }

    chip->to_irq = max7311_gpio_to_irq;

    for (j = 0; j < ts->chip.ngpio; j++) {
        if(ts->port_config & (1 << j)) {
            irq = irq_create_mapping(ts->irq_domain, j);
            irq = irq_create_mapping(ts->irq_domain, j);
            irq_set_lockdep_class(irq, &gpio_lock_class, &gpio_lock_class);
            irq_set_chip_data(irq, ts);
            irq_set_chip(irq, &max7311_irq_chip);
            irq_set_nested_thread(irq, true);
            irq_set_noprobe(irq);
        }
    }

    return 0;
}

static void max7311_irq_teardown(struct max7311 *ts)
{
    int i, irq;
    struct i2c_client *client = to_i2c_client(ts->dev);
    if (ts->irq_base)
        free_irq(client->irq, ts);

    for (i = 0; i < ts->chip.ngpio; ++i) {
        irq = irq_find_mapping(ts->irq_domain, i);
        if (irq > 0)
            irq_dispose_mapping(irq);
    }
    irq_domain_remove(ts->irq_domain);
}

static struct max7311_platform_data *max7311_get_devtree_pdata(struct device *dev)
{
    struct max7311_platform_data *pdata;
    struct device_node* np = dev->of_node;

    pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);

    if(pdata == NULL) {
        dev_err(dev, "Can't allocate platform data memory\n");
        return -ENOMEM;
    }
    if (of_property_read_u32(np, "port1-config", &pdata->port1_config) ) {
        dev_err(dev, "port1-config missed\n");
        return -EINVAL;
    }
    if (of_property_read_u32(np, "port2-config", &pdata->port2_config) ) {
        dev_err(dev, "port2-config missed\n");
        return -EINVAL;
    }
    if (of_property_read_u32(np, "out1-default", &pdata->out1_default) ) {
        dev_err(dev, "out1-default missed\n");
        return -EINVAL;
    }
    if (of_property_read_u32(np, "out2-default", &pdata->out2_default) ) {
        dev_err(dev, "out2-default missed\n");
        return -EINVAL;
    }

    return pdata;
}

int max7311_gpio_config(struct max7311 *ts)
{
    struct device *dev = ts->dev;
    struct i2c_client *client = to_i2c_client(dev);
    struct max7311_platform_data *pdata;
    int ret, value;

    pdata = dev->platform_data;
    if(!pdata) {
        dev_info(dev, "Probing device tree ...\n");
        pdata = max7311_get_devtree_pdata(dev);
        if (IS_ERR(pdata)) {
            dev_err(dev, "Get device tree data failed\n");
            return PTR_ERR(pdata);
         }
    }

    mutex_init(&ts->lock);

    ts->out_level   = (pdata->out2_default << 8)    | pdata->out1_default;
    ts->port_config = (pdata->port2_config << 8)    | pdata->port1_config;

    dev_set_drvdata(dev, ts);

    ts->chip.label  = dev->driver->name;
    ts->chip.direction_input = max7311_direction_input;
    ts->chip.get = max7311_get;
    ts->chip.direction_output = max7311_direction_output;
    ts->chip.set = max7311_set;
    ts->chip.base = pdata->base;
    ts->chip.ngpio = PIN_NUMBER;
    ts->chip.can_sleep = 0;
    ts->chip.parent = dev;
    ts->chip.owner = THIS_MODULE;

    max7311_setb(ts, MUX_REG_RW_PORT1_OUT, pdata->out1_default);
    max7311_setb(ts, MUX_REG_RW_PORT2_OUT, pdata->out2_default);

    max7311_setb(ts, MUX_REG_RW_PORT1_CONFIG, pdata->port1_config);
    max7311_setb(ts, MUX_REG_RW_PORT2_CONFIG, pdata->port2_config);

    ret = gpiochip_add(&ts->chip);
    if (ret)
        goto exit_destroy;

    /* IRQ Setup */
    ts->irq_base = client->irq;
    if(client->irq > 0) {
        ret = max7311_irq_setup(ts);
            if (ret) {
                max7311_irq_teardown(ts);
                goto exit_destroy;
            }
    }

    value = max7311_getb(&ts->chip, 1);
    ts->in_level = (max7311_getb(&ts->chip, 2) << 8) | value;

    return ret;

exit_destroy:
    dev_set_drvdata(dev, NULL);
    mutex_destroy(&ts->lock);
    return ret;
}

static int max7311_probe(struct i2c_client *client,
             const struct i2c_device_id *id)
{
    struct max7311 *ts;
    u8 value;
    int ret;

    ts = devm_kzalloc(&client->dev, sizeof(struct max7311), GFP_KERNEL);
    if (!ts)
        return -ENOMEM;

    ts->read = max7311_i2c_read;
    ts->write = max7311_i2c_write;
    ts->dev = &client->dev;

    value = max7311_getb(&ts->chip, 1);
    ts->in_level = (max7311_getb(&ts->chip, 2) << 8) | value;

    i2c_set_clientdata(client, ts);

    ret = max7311_gpio_config(ts);

    if (!ret)
        dev_info(ts->dev, "Driver version 1.1.0\n");

    return ret;
}

static int max7311_remove(struct i2c_client *client)
{
    struct max7311 *ts = dev_get_drvdata(&client->dev);
    int ret;

    if (ts == NULL)
        return -ENODEV;

    max7311_irq_teardown(ts);
    dev_set_drvdata(&client->dev, NULL);

    gpiochip_remove(&ts->chip);
    mutex_destroy(&ts->lock);
    ret = 0;

    return ret;
}

static const struct i2c_device_id max7311_id[] = {
    { "max7311", 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, max7311_id);

static const struct of_device_id max7311_of_table[] = {
    { .compatible = "maxim,max7311atg", },
    { .compatible = "maxim,max7311",    },
    { }
};
MODULE_DEVICE_TABLE(of, max7311_of_table);

static struct i2c_driver max7311_driver = {
    .driver = {
        .name           = "max7311",
        .owner          = THIS_MODULE,
        .of_match_table = max7311_of_table,
    },
    .probe      = max7311_probe,
    .remove     = max7311_remove,
    .id_table   = max7311_id,
};

static int __init max7311_init(void)
{
    return i2c_add_driver(&max7311_driver);
}

static void __exit max7311_exit(void)
{
    i2c_del_driver(&max7311_driver);
}

subsys_initcall(max7311_init);
module_exit(max7311_exit);

MODULE_AUTHOR("ArtyPi");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("MAX7311 port-expander");












